# API Design document 

This conference app needs to adapt to multiple services used by a conference

We use OAuth2 (PKCE) for identifying the user through one of the supported Identity Provider (i.e. Google, Twitter, GitHub)

## Table of Contents
- [Feature Driven Development](#feature-driven-development)
- [Feature specific API](#feature-specific-api)
- [Conference APIs](#conference-api)
- [Example of possible Features](#example)
  - [Talks](#talks)
  - [Socializing](#socializing)
  - [Speakers](#speakers)


## Feature Driven Development
We use Feature Driven Development, for all the different functionalities we implement.

Each Feature has a public endpoint that describe:
- if it is enabled/disable (error calling the api means it is disabled)
- what are the activities supported by the server (they may differ from those implemented in the client app, only those supported both sides will be available)
- Other endpoints to be used for different feature activites
- if the Feature needs to identify the user and which Identity Providers are supported

For each Feature the conference app may have:
- a <feature> specific folder in the src/app/feature/ folder
- a <feature>-service.ts file that when the application is started initialize the feature config and make it available to the app
- a <feature>-api.ts file to expose a limited set of method calls to be used by the app
- a <feature>-model.ts file describing the structure of the returned data, this model is then used inside the conference app
- a <feature>-state.ts to keep track of what is happening in the app
- a component folder to share Angular dumb components (Graphical elements with no state)


## Feature specific API

Since each organizer may want to use features offered by different providers and each feature has different SLA, we need to find a resilient way to integrate them.

_Note:_ When the conference app is used inside the browser (PWA version) it can only call APIs in the same domain where the app is downloaded from. Any call to other domains will result in a CORS error (browser is executing the call but blocking the response). To allow calls to external APIs, the API has to have the allow-cors-* header (TODO: fix the header name and value)  in the response. Alternatively the /api endpoint could do a server-side proxying to the external domain and return the results to the client.

## Conference APIs
The conference app needs a set of basic APIs to retrieve the minimal set of data in order to work.
These APIs endpoints are all exposed on the same domain from where the conference application is downloaded

``` Example single conference: 
https://vdz20.cfp.dev/app/index.html is the entry url for the conference app of Voxxed Days Zurich 2020
https://vdz20.cfp.dev/api/conferences is the first endpoint that will be called by the application when it is launched to download an array of conference data (conferenceId, name, description (in english), location, startDate, endDate, timezone, coordinates, scheduleUrls, talksUrl, speakersUrl)
```

``` Example multiple conferences: 
https://devoxxians.com/app/index.html : is the entry url for the all Devoxx and Voxxed conferences 
https://devoxxians.com/api/conferences : is the first endpoint that will be called by the application when it is launched to download an array of conference data (Response Model: conferenceId, name, description (in english), location, startDate, endDate, timezone, coordinates, scheduleUrls: string[], talksUrl, speakersUrl)
```

/api/conferences : returns an array of conference data (Response Model: conferenceId, name, description (in english), location, startDate, endDate, timezone, coordinates, scheduleUrls: string[], talksUrl, speakersUrl, lastUpdateTimeUTC)
_Note:_ lastUpdateTime is used to inform the conference app that something is changed (i.e. speaker profile change, a talk has changed room, etc) and all the data for the conference needs to be reloaded

/api/conferences/{conferenceId} : same response as the /api/conferences but without the array (only 1 conference)

/api/conferences/{conferenceId}/talks : (a.k.a. talksUrl) returns all the talks (un-ordered) that have been Approved by the Program Committee and Accepted by the speaker(s) **also if they are not yet scheduled** (Response Model: talkId, title, talkLanguage, abstract, speakers (Model: speakerId, displayName, company, twitter))

/api/conferences/{conferenceId}/talks/{talksId} : (a.k.a. talkUrl) returns the talk details (Response Model: talkId, title, talkLanguage, abstract, speakers (Model: speakerId, displayName, company, twitter), scheduledRoom, scheduledTime, scheduleUrl)

/api/conferences/{conferenceId}/speakers : (a.k.a. speakersUrl) returns all the speakers (alphabetically ordered) that have at least one talk in the talksUrl. (Response Model: speakerId, displayName, firstname, surname, company, photoUrl)

/api/conferences/{conferenceId}/speakers/{speakerId} : (a.k.a. speakerUrl) returns the speaker details (Response Model: speakerId, displayName, firstname, surname, company, twitter, photoUrl, bio)

/api/conferences/{conferenceId}/schedules : return the same scheduleUrls array that we get in the /api/conferences endpoint. The array might be empty if the schedule is not yet published.

/api/conferences/{conferenceId}/schedules/{scheduleId} : (a.k.a. scheduleUrl) returns the whole schedule for 1 day (same structure as old CFP API) 
_Note:_ date and values are passed as string formatted using the conference TimeZone, the used TimeZone is also returned 

/api/conferences/{conferenceId}/schedules/{scheduleId}/{roomId}: (a.k.a. roomScheduleUrl) returns the whole schedule for 1 day but only for 1 specific room (used by digital-signage-app)
_Note:_ date and values are passed as string formatted using the conference TimeZone, the used TimeZone is also returned 





## Example of possible Features (Brainstorming)


* Link your ticket
  - Many features requires you have a ticket linked
  - show your ticket QR code for the checkin
  - We know when you check in
  - You can socialize with others that scans your barcode


* Welcome message after you checkin
* Scan your wardrobe code (in case you lose physical key)
* Add to Favorites
* Vote talk (last 15 min and till 2h later) with feedbacks
* Clap during the talk, finger down during the talk (with options: offensive/inappropiate, I don't agree, I'm getting bored)
* Give feedbacks to conference organizers, almost in real-time
* My Schedule (1 talk per time slot, notify 10min before the talk, reminds you to vote)
* Scan an attendee badge to get his contact (name, surname & chat within the app)
* Contacts handling: become friends, create a group of friends
* Chat with friends and groups
* Share your pictures on the event Flickr page
* Tweet about the talk you are following (with pictures) (automatic link to the talk page)
* Socializing:
  - Find a buddy
  - Share a taxi: from the Airport to the event or city center, everyday to/back the conference
  - Go out for dinner with somebody
  - Share your profile to unknown people
  - Share yopur position with a friend or a group (for 15 min)

* Speaker features:
  - get info about the speaker and staff dinner
  - read feedbacks and votes for your talk
  - get the reminder to go to test your equipment
  - get the reminder where is your talk


* Lost & Found 
