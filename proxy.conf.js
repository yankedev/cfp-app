const generateProxyElement = function (cfpKey, cfpTargetHost) {
  // proxy middleware options
  var options = {
    context: ["/api/conferences/"+cfpKey],
    target: cfpTargetHost, // target host
    changeOrigin: true, // needed for virtual hosted sites
    secure: false,
    logLevel: "debug",
    ws: true, // proxy websockets
    pathRewrite: {
      ['^/api/conferences/'+cfpKey] : '/api' // remove base path
    }
  };

  return options;
}

const PROXY_CONFIG = [
  {
    context: ["/api/public/events/"], 
      target: "https://www.devoxxians.com/",
      secure: false,
      logLevel: "debug",
      changeOrigin: true
  }
];

PROXY_CONFIG.push(generateProxyElement('dvbe19', 'https://staging.cfp.dev'));
//PROXY_CONFIG.push(generateProxyElement('dvbe19', 'http://localhost:8080'));
//PROXY_CONFIG.push(generateProxyElement('dvbe19', 'https://dvbe19.cfp.dev'));
PROXY_CONFIG.push(generateProxyElement('unvoxxedhawaii', 'https://unvoxxedhawaii.cfp.dev'));
PROXY_CONFIG.push(generateProxyElement('vdz20', 'https://vdz20.cfp.dev'));
PROXY_CONFIG.push(generateProxyElement('vxdbuh20', 'https://vxdbuh20.cfp.dev'));
PROXY_CONFIG.push(generateProxyElement('vdm20', 'https://vdm20.cfp.dev'));
PROXY_CONFIG.push(generateProxyElement('devoxxfr20', 'https://devoxxfr20.cfp.dev'));
PROXY_CONFIG.push(generateProxyElement('devoxxuk20', 'https://devoxxuk20.cfp.dev'));
PROXY_CONFIG.push(generateProxyElement('devoxxpl20', 'https://devoxxpl20.cfp.dev'));

module.exports = PROXY_CONFIG;
