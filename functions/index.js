/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();
const express = require('express');
const cookieParser = require('cookie-parser')();
const cors = require('cors')({origin: true});
const proxy = require('http-proxy-middleware');
const app = express();

const generateProxyOptions = function (cfpKey, cfpTargetHost) {

  // proxy middleware options
  var options = {
    target: cfpTargetHost, // target host
    changeOrigin: true, // needed for virtual hosted sites
    secure: false,
    logLevel: "debug",
    ws: true, // proxy websockets
    pathRewrite: {
      // localhost:5000 redirect to /app/api, thus we need to remove everything before /api when calling the real cfp instance
      ['^.*/api/conferences/'+cfpKey] : '/api' 
    }
  };

  return proxy(options);
}

// Express middleware that validates Firebase ID Tokens passed in the Authorization HTTP header.
// The Firebase ID token needs to be passed as a Bearer token in the Authorization HTTP header like this:
// `Authorization: Bearer <Firebase ID Token>`.
// when decoded successfully, the ID Token content will be added as `req.user`.
const validateFirebaseIdToken = async (req, res, next) => {
  console.log('Check if request is authorized with Firebase ID token');

  if ((!req.headers.authorization || !req.headers.authorization.startsWith('Bearer ')) &&
      !(req.cookies && req.cookies.__session)) {
    console.error('No Firebase ID token was passed as a Bearer token in the Authorization header.',
        'Make sure you authorize your request by providing the following HTTP header:',
        'Authorization: Bearer <Firebase ID Token>',
        'or by passing a "__session" cookie.');
    res.status(403).send('Unauthorized');
    return;
  }

  let idToken;
  if (req.headers.authorization && req.headers.authorization.startsWith('Bearer ')) {
    console.log('Found "Authorization" header');
    // Read the ID Token from the Authorization header.
    idToken = req.headers.authorization.split('Bearer ')[1];
  } else if(req.cookies) {
    console.log('Found "__session" cookie');
    // Read the ID Token from cookie.
    idToken = req.cookies.__session;
  } else {
    // No cookie
    res.status(403).send('Unauthorized');
    return;
  }

  try {
    const decodedIdToken = await admin.auth().verifyIdToken(idToken);
    console.log('ID Token correctly decoded', decodedIdToken);
    req.user = decodedIdToken;
    next();
    return;
  } catch (error) {
    console.error('Error while verifying Firebase ID token:', error);
    res.status(403).send('Unauthorized');
    return;
  }
};

var optionsDevoxxians = {
  target: 'https://www.devoxxians.com', // target host
  changeOrigin: true, // needed for virtual hosted sites
  secure: false,
  logLevel: "debug",
  ws: true, // proxy websockets
};
app.use(cors);
app.use(cookieParser);
app.use('/hello', validateFirebaseIdToken);
app.get('/hello', (req, res) => {
  res.send(`Hello ${req.user.name}`);
});
app.use('/api/vote', validateFirebaseIdToken);

app.use('/api/conferences/dvbe19', generateProxyOptions('dvbe19', 'https://staging.cfp.dev'));
app.use('/api/conferences/vdz20', generateProxyOptions('vdz20', 'https://vdz20.cfp.dev'));
app.use('/api/conferences/unvoxxedhawaii', generateProxyOptions('unvoxxedhawaii', 'https://unvoxxedhawaii.cfp.dev'));
app.use('/api/conferences/vxdbuh20', generateProxyOptions('vxdbuh20', 'https://vxdbuh20.cfp.dev'));
app.use('/api/conferences/vdm20', generateProxyOptions('vdm20', 'https://vdm20.cfp.dev'));
app.use('/api/conferences/devoxxfr20', generateProxyOptions('devoxxfr20', 'https://devoxxfr20.cfp.dev'));
app.use('/api/conferences/devoxxuk20', generateProxyOptions('devoxxuk20', 'https://devoxxuk20.cfp.dev'));
app.use('/api/conferences/devoxxpl20', generateProxyOptions('devoxxpl20', 'https://devoxxpl20.cfp.dev'));

app.use('/api/public/events', proxy(optionsDevoxxians));


// This HTTPS endpoint can only be accessed by your Firebase Users.
// Requests need to be authorized by providing an `Authorization` HTTP header
// with value `Bearer <Firebase ID Token>`.
exports.app = functions.https.onRequest(app);

// ===========================
exports.voteUpdatedAt = functions.firestore.document('conferences/{eventId}/sessions/{sessionId}/users/{userId}').onUpdate((change, context) => {
  let data = change.after.data();
  // let previousData = change.before.data();
  let now = new Date().getTime();

  if (data.updatedAt > (now - (30 * 1000))) {
    return
  }

  return change.after.ref.set({ 'updatedAt': now }, { merge: true })
});

exports.aggregateRatingsOnNewVote = functions.firestore
    .document('conferences/{eventId}/sessions/{sessionId}/users/{userId}')
    .onCreate((snap, context) => {
      // add creation time to the vote!
      snap.ref.set({ 'createdAt': new Date().getTime() }, { merge: true });
      // Get value of the newly added rating
      var ratingVal = snap.data().rating;
      var sessionId = context.params.sessionId;
      // Get a reference to the session
      var sessionRef = admin.firestore().collection('conferences')
                      .doc(context.params.eventId)
                      .collection('sessions')
                      .doc(sessionId);

      // Update aggregations in a transaction
      return admin.firestore().runTransaction(transaction => {
        return transaction.get(sessionRef).then(sessionDoc => {
          var session = sessionDoc.data();
          
          if (!session){
            console.log('first vote and creating new session with id: '+sessionId);
            return transaction.set(sessionRef, {
              avgRating: ratingVal,
              numRatings: 1
            }, { merge: true });
          } else {
            console.log('calculating new average vote for session with id: '+sessionId);
            // if session exist it means we already have some votes
            // Compute new number of ratings
            var newNumRatings = session.numRatings + 1;

            // Compute new average rating
            var oldRatingTotal = session.avgRating * session.numRatings;
            var newAvgRating = (oldRatingTotal + ratingVal) / newNumRatings;
          
            // Update session info
            return transaction.set(sessionRef, {
              avgRating: newAvgRating,
              numRatings: newNumRatings
            }, { merge: true });
          }
        });
      });
    });

    exports.aggregateRatingsOnUpdateVote = functions.firestore
      .document('conferences/{eventId}/sessions/{sessionId}/users/{userId}')
      .onUpdate((change, context) => {
        var sessionId = context.params.sessionId;
        console.log('updating vote for session with id: '+sessionId);
            
        var oldRatingVal = change.before.data().rating;
        // Get value of the newly added rating
        var ratingVal = change.after.data().rating;

        // Get a reference to the session
        var sessionRef = admin.firestore().collection('conferences')
                        .doc(context.params.eventId)
                        .collection('sessions')
                        .doc(context.params.sessionId);

        // Update aggregations in a transaction
        return admin.firestore().runTransaction(transaction => {
          return transaction.get(sessionRef).then(sessionDoc => {
            // Number of ratings does not change
            var numRatings = sessionDoc.data().numRatings;

            // Compute new average rating
            var oldRatingTotal = sessionDoc.data().avgRating * numRatings;
            var newAvgRating = (oldRatingTotal - oldRatingVal + ratingVal) / numRatings;
            console.log('oldRatingTotal: '+oldRatingTotal+' - oldRatingVal: '+oldRatingVal+' + ratingVal: '+oldRatingVal +' / numRatings: '+numRatings);
            console.log('new avgRating for session with id '+sessionId+': '+newAvgRating);
        
            // Update restaurant info
            return transaction.set(sessionRef, {
              avgRating: newAvgRating
            }, { merge: true });
          });
        });
    });
