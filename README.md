# Conference Application

This application is used by Devoxx and Voxxed Days developer conferences but is completely free and available to everybody that wants to fork and customized it for its needs thanks to the Apache Software License 2.0

This repository is a mirror of the "official" [Ionic conference-app](https://github.com/ionic-team/ionic-conference-app/). We mirror their master branch in order to get their continuous updates to the lastest version of their library. If we solve any bug or issue we try to contribute back directly on their repository submitting a pull-request.
We add all our customization and features in our branch and we try to keep easy to merge changes without changing the application structure too much. (Because Ionic team is doind a great job in keeping all the dependencies up-to-date and improving their code continuosly)

## Table of Contents
- [Getting Started](#getting-started)
- [Contributing](#contributing)
- [App Preview](#app-preview)
- [Deploying](#deploying)
  - [Progressive Web App](#progressive-web-app)
  - [Android](#android)
  - [iOS](#ios)


## Getting Started

* [Download the installer](https://nodejs.org/) for Node.js 6 or greater.
* Install the ionic CLI globally: `npm install -g ionic`
* Clone this repository: `git clone https://gitlab.com/yankedev/cfp-app.git`.
* Run `npm install` from the project root.
* Run `ionic serve` in a terminal from the project root.
* Profit. :tada:

_Note: See [How to Prevent Permissions Errors](https://docs.npmjs.com/getting-started/fixing-npm-permissions) if you are running into issues when trying to install packages globally._

## Contributing
**Any contribution is welcome** We started this project so that every Devoxx and Voxxed days conference organizer was able to fix problems and add features he/she needed. In each organization team we always have many skilled developers taking care of everything that is needed to run a conference for their friends and local geeks. Sometimes it can be hard for a developer to spend much time sending mails, organizing catering and checking the budget, thus the idea of having the opportunity to improve the conference showing our own (and best) skills was needed to decompress from the stress behind these events :-)
If you want to contribute to improve our events, have fun with friends, show your competences, you are welcome to join our initiative. Anything counts: write code, fix a bug, improve documentation, add a test case and we will give you credits in the release notes and during the opening session of our events.
We are trying to help junior developer to get better, so don't be shy and get your hands dirty with issues labelled as "First time issue"

If conference organizers need to fix a problem before their event but they don't have resources available because event logistics are  more important, they may allocated a budget to be distributed to contributors willing to help. Issues labeled with "VoxxedPoints: 30" mean that the developer submitting a PR to fix it, will get 30€ discount on the ticket price for any of the Devoxx and VoxxedDays events.

Mentors helping junior developers, answering questions on issue, reviewing their PR and recommending improvements before the PR gets merged will also be mentioned on our git repo.

## App Preview

All app preview screenshots were taken by running `ionic serve --lab` on a retina display.

- [Schedule Page](https://github.com/ionic-team/ionic-conference-app/blob/master/src/app/pages/schedule/schedule.html)

  <img src="resources/screenshots/SchedulePage.png" alt="Schedule">


- [About Page](https://github.com/ionic-team/ionic-conference-app/blob/master/src/app/pages/about/about.html)

  <img src="resources/screenshots/AboutPage.png" alt="Schedule">


- To see more images of the app, check out the [screenshots directory](https://github.com/ionic-team/ionic-conference-app/tree/master/resources/screenshots)!


## Deploying

### Progressive Web App

We use firebase to run the PWA

*Setup firebase*
1. Run `npm i -g firebase-tools`
2. Run `firebase login`
3. Check the name of your project in firebase console and use it in .firebaserc and functions/index.js
3. Run `cd functions; npm install; cd -`

*Test PWA locally*
1. Run `ng build --prod`
2. Run `firebase serve --only hosting,functions`
3. Open the browser at `localhost:5000`

*Deploy in production*
1. Run `ng build --prod`
2. Run `firebase deploy --only hosting,functions`


### Android

1. Run `ionic cordova run android --prod`

### iOS

1. Run `ionic cordova run ios --prod`
