describe('License plate store home page', () => {

  beforeEach(() => {
    cy.server();
    cy.route('/api/public/events/past', []).as('pastconferences');
    cy.route('/api/public/events/upcoming', [ {
        "id": 67,
        "name": "Voxxed Days Ticino 2019",
        "eventCategory": "VOXXED",
        "fromDate": "2019-10-05T08:00:00Z",
        "toDate": "2019-10-05T17:00:00Z",
        "imageURL": "https://s3-eu-west-1.amazonaws.com/voxxeddays/webapp/images/8d53e580-394c-491e-9eb7-5d18dec57055.jpg",
        "website": "https://voxxeddays.com/ticino",
        "apiURL": "https://cfpvdt19.confinabox.com/api/",
        "cfpKey": "cfpvdt19"
      }, {
        "id": 59,
        "name": "Devoxx Belgium 2019",
        "eventCategory": "DEVOXX",
        "fromDate": "2019-11-04T08:30:00Z",
        "toDate": "2019-11-08T12:30:00Z",
        "imageURL": "https://s3-eu-west-1.amazonaws.com/voxxeddays/webapp/images/654808ec-68e8-48b9-87dd-74570b7c5649.jpg",
        "website": "https://devoxx.be",
        "apiURL": "https://dvbe19.cfp.dev/api/",
        "cfpKey": "dvbe19"
      }]
    ).as('conferences');
    cy.route('/api/conferences/dvbe19/public/speakers', 'fixture:dvbe19/speakers.json').as('speakers');
    cy.visit('/conference-selector');
    cy.wait('@conferences');
    //cy.wait('@pastconferences');
    //cy.wait('@speakers');
  });

  
  it('displays the right main title', () => {
    cy.contains("Conference Selector")
      .should('be.visible')
      .should('have.css', 'font-weight', '400')
      .should('have.css', 'font-size', '16px')
      .and('have.css', 'font-family').and('match', /Roboto/);
  });

  it('can navigate to select Devoxx Belgium and find speakers', () => {
    cy.get('[value="devoxx"]').click();
    cy.get('.eventsCard > .fab-horizontal-end > .ion-activatable > .md').click();
    //cy.contains('My Cart').should('be.visible');
    cy.contains('Speakers').should('be.visible');
    cy.contains('Alberto Rios').should('be.visible');
    cy.checkSpeakerAt(1, 'Aayush Arora', 'Roobits');
  });


});
