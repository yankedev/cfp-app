import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})

export class ToastService {

  constructor(
    public toastController: ToastController
  ) { }

  async showToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      position: 'bottom',
      showCloseButton: true,
      duration: 3000,
      closeButtonText: 'Done'
    });

    return toast.present();
  }

  async showSuccessToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      position: 'bottom',
      showCloseButton: false,
      duration: 2000,
      color: 'success'
    });

    return toast.present();
  }

}
