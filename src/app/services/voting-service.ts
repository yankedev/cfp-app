import { Injectable } from '@angular/core';

import {
  AngularFirestore,
  AngularFirestoreDocument,
  AngularFirestoreCollection,
  DocumentReference
} from '@angular/fire/firestore';

import { take } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class VotingService {
    confRef: AngularFirestoreCollection;

  constructor(private afs: AngularFirestore) {
    // Reference database location for conferences information
    this.confRef = this.afs.collection('conferences');
  }

  // Event //
  createConference(
    eventId: string,
    eventName: string,
    eventDescription: string,
    eventLocation: string,
    eventDate: string,
    eventType: 'DEVOXX' | 'VOXXED'
  ): Promise<DocumentReference> {
    return this.confRef.add({
        id: eventId,
        name: eventName,
        description: eventDescription,
        location: eventLocation,
        date: eventDate,
        type: eventType
      });
  }

  getConferences(eventType: string): AngularFirestoreCollection<Event> {
    return this.afs.collection('conferences', ref => ref.where('type', '==', eventType));
  }

  vote(eventId: string,
       sessionId: string,
       userId: string,
       vote: number,
       comment?: string): Promise<void> {
    return this.afs.doc(`conferences/${eventId}/sessions/${sessionId}/users/${userId}`).set({
      rating: vote, comment: comment
    }, {'merge': true});
  }

  getVote(eventId, sessionId, userId) {
    return this.afs.doc(`conferences/${eventId}/sessions/${sessionId}/users/${userId}`).valueChanges();
  }
}
