import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { IonicModule } from '@ionic/angular';
import { IonicStorageModule } from '@ionic/storage';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { ConferenceData } from './providers/confinabox-data';
import { ConferenceSelector } from './providers/conference-selector';
import { httpInterceptorProviders } from './http-interceptors';
import { SpeakersData } from './providers/speakers-data';
import { AuthService } from './providers/auth-service';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AuthGuard } from './core/auth-guard';
import { ConferenceService } from './providers/conference-service';
import { VotingService } from './services/voting-service';
import { ToastService } from './services/toast.service';

@NgModule({
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule.enablePersistence(),
    AppRoutingModule,
    HttpClientModule,
    IonicModule.forRoot(),
    IonicStorageModule.forRoot({
      name: '__cfp',
      driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: !environment.developmentMode
    })
  ],
  declarations: [AppComponent],
  providers: [AuthGuard, AuthService, SpeakersData, ConferenceData, ConferenceService, ConferenceSelector,
              VotingService, ToastService, InAppBrowser, SplashScreen, StatusBar, httpInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule {}
