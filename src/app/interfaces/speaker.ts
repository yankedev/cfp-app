import { Session } from './session';
import { Injectable } from '@angular/core';
import { Adapter } from '../core/adapter';

export class Speaker {

  bio: string;
  imageURL?: string; // URL to a photo
  twitter?: string;
  about?: string;
  company?: string;
  location?: string;
  phone?: string;
   // Unique String identifier
  sessions?: Session[];
  link: object; // multiple links

  constructor(public id: string, public firstName: string, public lastName: string) {}

  getDisplayName(): string {
    if (this.firstName && this.lastName) {
      return this.firstName + ' ' + this.lastName;
    } else if (this.firstName) {
      return this.firstName;
    } else if (this.lastName) {
      return this.lastName;
    } else {
      return 'Unknown Speaker';
    }
  }
}

