import { Session } from './session';
export class Group {
  fromTime: string;
  toTime: string;
  time: string;
  day: string;
  roomName: string;
  sessions: Session[];

  constructor(day: string, time: string) {
    this.day = day;
    this.fromTime = time;
    this.sessions = [];
  }
}
