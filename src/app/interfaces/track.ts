export class Track {
  id: string;
  title: string;
  description: string;
  iconUrl: string;
}
