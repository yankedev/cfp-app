
export class TimeSlot {
    roomId: string;
    roomName: string;
    timeStart: string; // time format 2018-11-19T11:00:00Z
    timeEnd: string; // time format 2018-11-19T11:50:00Z
    timeZone: string; // zone format Europe/Brussels"
  }