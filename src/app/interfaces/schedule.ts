import { Group } from './group';

/*
 * Schedule is a logical entity, it can be persisted will all the data for one day or it can be generated at runtime
 * after executing some filtering
 */
export class Schedule {
  date: string; // localtime
  groups: Group[]; // each group is a timeline
  shownSessions: number;

  constructor(date: string) {
    this.date = date;
    this.groups = [];
  }
}
