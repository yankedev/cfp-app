import { Speaker } from './speaker';
import { TimeSlot } from './time-slot';

export class Talk {
  id: string;
  title: string;
  lang: string;
  summary: string; //description
  trackId: string;
  track: string;
  type: string; // sessionTypeName
  durationInMinutes: number; // sessionTypeDuration
  timeSlots: TimeSlot[];
  summaryAsHtml: string;
  speakers: Speaker[];
  tags: string[];
}
