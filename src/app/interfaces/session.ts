import { Speaker } from './speaker';

export class Session {
  name: string;
  timeStart: string;
  timeEnd: string;
  day: string;
  location: string;
  tracks: string[];
  id: string;
  description: string;
  speakerNames: string[];
  speakers: Speaker[];
  type: string;

  constructor(id: string, description: string) {
    this.id = id;
    this.description = description;
    this.tracks = [];
    this.speakerNames = [];
    this.speakers = [];
  }
}
