export class EventDetails {
  id?: number;
  code?: string;
  website?: string;
  description?: string;
  imageURL?: string;
  faqURL?: string;
  codeOfConductURL?: string;
  presentationTemplateURL?: string;
  youTubeURL?: string;
  eventImagesURL?: string;
  fromDate?: string;
  toDate?: string;
  locationName?: string;
  timezone?: string;
  eventCategory?: string;

  constructor(public cfpKey: string,
              public name: string, public apiUrl: string) {}
}
