import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { auth } from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import {
  AngularFirestore,
  AngularFirestoreDocument
} from '@angular/fire/firestore';

import { Observable, of } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { User } from '../interfaces/user';
import { Events } from '@ionic/angular';

@Injectable({ providedIn: 'root' })
export class AuthService {
  user$: Observable<User>;
  public userId: string;
  public idToken: string;

  constructor(
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private router: Router,
    private events: Events
  ) {
    this.user$ = this.afAuth.authState.pipe(
      switchMap(user => {
        if (user) {
          // Logged in
          this.userId = user.uid;
          user.getIdToken().then(token => this.idToken = token);
          return this.afs.doc<User>(`users/${user.uid}`).valueChanges();
        } else {
          this.userId = null;
          this.idToken = null
          // Logged out
          return of(null);
        }
      })
    );
    this.user$.subscribe();
  }
  public getCurrentUserId(): string {
    return this.userId;
  }

  async googleSignin() {
    this.signin(new auth.GoogleAuthProvider());
    this.router.navigate(['/']);
  }

  async githubSignin() {
    this.signin(new auth.GithubAuthProvider());
    this.router.navigate(['/']);
  }

  async twitterSignin() {
    this.signin(new auth.TwitterAuthProvider());
    this.router.navigate(['/']);
  }

  async signin(provider: firebase.auth.AuthProvider) {
    const credential = await this.afAuth.auth.signInWithPopup(provider);
    await this.events.publish('user:login');
    return this.updateUserData(credential.user, provider.providerId);
  }

  private updateUserData(user: firebase.User, provider: string) {
    // Sets user data to firestore on login
    const userRef: AngularFirestoreDocument<User> = this.afs.doc(
      `users/${user.uid}`
    );

    const data: User = {
      id: user.uid,
      email: user.email,
      name: user.displayName,
      picture: user.photoURL,
      username: user.uid,
      provider: provider
    };

    return userRef.set(data, { merge: true });
  }

  async signOut() {
    await this.afAuth.auth.signOut();
    await this.events.publish('user:logout');
    this.router.navigate(['/']);
  }

  getFirebaseToken() {
    if (this.userId){
      return this.idToken;
    }
    return null;
  }
}
