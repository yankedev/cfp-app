import { HttpClient } from '@angular/common/http';
import { Injectable, OnInit } from '@angular/core';
import { Observable, Subject, of, forkJoin, BehaviorSubject } from 'rxjs';
import { map, tap, mergeMap, distinct, take } from 'rxjs/operators';

import { EventDetails } from '../interfaces/eventDetails';
import { UserData } from './user-data';

@Injectable({ providedIn: 'root' })
export class ConferenceSelector {
  private FUTURE_EVENT_LIST_URL = '/api/public/events/upcoming';
  private PAST_EVENT_LIST_URL = '/api/public/events/past';

  // contains the event object of the currently selected event
  private selectedEvent$ = new BehaviorSubject<EventDetails>(null);
  private availableEvents: EventDetails[];
  private eventList$: Observable<EventDetails[]>;
  toggle = 1;

  constructor(public http: HttpClient, private userData: UserData) {
    this.init();
  }

  init() {
    const futureEvents$ = this.http.get<EventDetails[]>(this.FUTURE_EVENT_LIST_URL);
    const pastEvents$ = this.http.get<EventDetails[]>(this.PAST_EVENT_LIST_URL);

    this.eventList$ = forkJoin([pastEvents$, futureEvents$]).pipe(
      map((events: EventDetails[][]) => events[0].concat(events[1])),
      map((events: any[]) => events.map(event => this.adaptDevoxxiansEvent(event))),
      tap(events => this.availableEvents = events)
    );
  }

  chooseEvent(eventId: string): Observable<EventDetails> {
    if (!this.availableEvents) {
      return this.eventList$.pipe(
        take(1),
        map(events => events.find(event => (event.cfpKey === eventId))),
        tap(foundEvent => this.setSelectedEvent(foundEvent))
      );
    } else {
      // if eventList has already been loaded, we (synchronously) select the right event
      const foundEvent = this.availableEvents.find(event => (event.cfpKey === eventId));
      return of(foundEvent).pipe(
        tap(event => this.setSelectedEvent(event))
      );
    }
  }

  setSelectedEvent(event: EventDetails) {
    if (!this.selectedEvent$.getValue() || this.selectedEvent$.getValue().cfpKey !== event.cfpKey) {
      this.selectedEvent$.next(event);
      // save selected event in userData, this is an async operation
      this.userData.storeSelectedEvent(event);
    }
  }

  getSelectedEvent$(): Observable<EventDetails> {
    return this.selectedEvent$.pipe(
      mergeMap(event => {
        if (event) {
          return of(event);
        } else {
          // if selectedEvent is null we search if there is an event stored in userdata
          return this.getStoredEventAndSetInObservable();
        }
      }),
      // since the event is set in the observable after if is retrieved from storage, we will have twice the same event
      distinct()
    );
  }

  private getStoredEventAndSetInObservable(): Observable<EventDetails> {
    return this.userData.getStoredEvent().pipe(
      tap(event => {
        if (event) { this.selectedEvent$.next(event); }
      }
    ));
  }

  getSelectedEvent(): EventDetails {
    return this.selectedEvent$.value;
  }

  getEventList(): Observable<EventDetails[]> {
    return this.eventList$;
  }

  // this method has hardcoded logic because the Devoxxians server returns a very specific data structure
  adaptDevoxxiansEvent(item: any): EventDetails {
    let apiUrl: string = item.apiURL;

    // apiUrl must not have the final /
    if (apiUrl && apiUrl.endsWith('/')) {
      apiUrl = apiUrl.slice(0, -1);
    }
    const event = new EventDetails(item.cfpKey, item.name, apiUrl);
    event.id = item.id;
    event.fromDate = item.fromDate;
    event.toDate = item.toDate;
    event.fromDate = item.fromDate;
    event.imageURL = item.imageURL;
    event.website = item.website;
    event.eventCategory = item.eventCategory;

    return event;
  }

  eventExist(event: EventDetails): Observable<boolean> {
    if (!event.apiUrl.startsWith('http') || !event.apiUrl.endsWith('/api')) {
      return of(false);
    }
    return of(true);
  }


  extractEventCode(url: string): string {
    if (!url) { return 'UNKNOWN_EVENT_URL'; }

    const matches = url.match('.*\://?([^\.]+)');
    if (!matches) { return 'UNKNOWN_EVENT_CODE'; }

    return matches[1];
  }
}
