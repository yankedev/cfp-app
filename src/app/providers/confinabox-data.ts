import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { of, forkJoin, Observable, BehaviorSubject, combineLatest } from 'rxjs';
import { map, mergeMap, tap, filter, retryWhen } from 'rxjs/operators';

import { UserData } from './user-data';

import { Session } from '../interfaces/session';
import { Speaker} from '../interfaces/speaker';
import { Schedule } from '../interfaces/schedule';
import { Group } from '../interfaces/group';
import { SpeakerAdapter } from '../core/speaker-adapter';
import { ConferenceSelector } from './conference-selector';
import { EventDetails } from '../interfaces/eventDetails';
import { Track } from '../interfaces/track';
import { TrackAdapter } from '../core/track-adapter';

@Injectable({ providedIn: 'root' })
export class ConferenceData {
  data: any;

  private URL_BASE_PATH = '/api/public';
  private SCHEDULES_URL = this.URL_BASE_PATH + '/schedules/';
  private TRACKS_URL = this.URL_BASE_PATH + '/tracks';

  private daysUrl = [];
  public eventCode: string;
  public daysUrl$: Observable<string[]>;
  public tracks$: Observable<Track[]>;
  public schedule$: Observable<any[]>;

  constructor(public http: HttpClient,
              public user: UserData,
              public conferenceSelector: ConferenceSelector,
              public speakerAdapter: SpeakerAdapter,
              public trackAdapter: TrackAdapter) {
    this.initData();
    this.init();
  }

  initData() {
    this.data = {};
    this.data.days = [];
    this.data.schedule = [];

    this.data.speakers = [];
    this.data.groups = [];
  }

  init() {

    this.daysUrl$ = this.conferenceSelector.getSelectedEvent$().pipe(
      filter(event => !!event),
      // since a new event has been selected we need to cleanup the schedule to regenerate it
      tap(event => this.initData()),
      mergeMap((event: EventDetails) => {
          return this.http.get(this.SCHEDULES_URL).pipe(
            retryWhen(errors$ => errors$.pipe(filter(err => err.status === '404')))
          );
        }
      ),
      map((schedule: any) =>
        schedule.links.map(link => link.href)
      ),
      tap(daysScheduleUrl => {
        this.daysUrl = daysScheduleUrl;
      })
    );

    this.tracks$ = this.conferenceSelector.getSelectedEvent$().pipe(
      filter(event => !!event),
      mergeMap((event: EventDetails) => {
        return this.http.get(this.TRACKS_URL);
      }),
      // old CFP returns track list in a map with key=tracks
      map((data: any) => data.tracks ? data.tracks : data),
      map((data: any[]) => data.map(item => this.trackAdapter.adapt(item)))
    );
    this.schedule$ = combineLatest([this.tracks$, this.daysUrl$]);
  }

  extractDay(link: string): string {
    return link;
  }

  getUrlsObservable(urls: string[]): Observable<Object>[] {
    return urls.map(url => {
      return this.http.get(url);
    });
  }

  load(): Observable<any> {
    if (this.data.schedule.length > 0) {
      return of(this.data);
    } else {
      return forkJoin(this.getUrlsObservable(this.daysUrl)).pipe(
        map((fullSchedule: any[]) => {
          fullSchedule.forEach((day, i) => {
            let dayName = this.extractDayName(this.daysUrl[i]);
            this.processData(dayName, day);
          });
          return this.data;
        })
      );
    }
  }

  extractDayName(dayUrl: string) {
    return dayUrl.substr(dayUrl.lastIndexOf('/')+1);
  }

  getDays() {
    return this.data.days;
  }

  processData(dayName: string, day: any) {
    // loop through each session
    // old CFP returns sessions of the day in an array named slots, 
    const slots = day.slots ? day.slots : day;
    slots.forEach((sessionData: any) => {
      // old CFP returns the day name inside each session data
      dayName = sessionData.day ? sessionData.day : dayName ;
      let sessionStartTime = sessionData.fromTime ? sessionData.fromTime : this.extractTime(sessionData.fromDate);
      let sessionEndTime = sessionData.toTime ? sessionData.toTime : this.extractTime(sessionData.toDate);
      // if not yet existing, create a schedule for this session date
      let daySchedule: Schedule = this.data.schedule.find(
        (s: Schedule) => s.date === dayName
      );
      if (!daySchedule) {
        daySchedule = new Schedule(dayName);
        this.data.schedule.push(daySchedule);
      }

      if (this.data.days.indexOf(daySchedule.date) === -1) {
        this.data.days.push(daySchedule.date);
      }

      // if not yet existing, create a group (a.k.a slot) for this session time (inside the schedule of the current day)
      let group: Group = daySchedule.groups.find(
        (g: Group) => g.fromTime === sessionStartTime
      );
      if (!group) {
        group = new Group(dayName, sessionStartTime);
        daySchedule.groups.push(group);
      }

      if (!sessionData.notAllocated) {
        // create a session with slot data, then it will be changed if it is a talk or a break
        let sessionId = sessionData.slotId ? sessionData.slotId : sessionData.id;
        let roomName = sessionData.roomSetup ? sessionData.roomSetup : sessionData.roomName;
        const session = new Session(sessionId, roomName);
        session.day = dayName;
        session.timeStart = sessionStartTime;
        session.timeEnd = sessionEndTime;
        session.location = sessionData.roomName;
        //new CFP
        session.type = sessionData.sessionTypeName;
        //old CFP
        if (!session.type) {
          session.type = 'Conference';
        }
        // insert session inside the current group
        group.sessions.push(session);

        if (sessionData.break || sessionData.sessionTypeBreak) {
          // old CFP
          if (sessionData.break) {
            session.id = sessionData.break.id;
            session.name = sessionData.break.nameEN;
            session.type = 'Break';
          } else {
            session.name = sessionData.sessionTypeName;
          }
        
          session.tracks.push('break');
        } else {
          if (sessionData.talk || sessionData.talkId) {
            session.id = sessionData.talkId ? sessionData.talkId : sessionData.talk.id;
            session.name = sessionData.talkTitle ? sessionData.talkTitle : sessionData.talk.title;
            session.description = sessionData.talk && sessionData.talk.summary ? sessionData.talk.summary : "No summary";
            
            if (sessionData.talk && sessionData.talk.track) {
              session.tracks.push(sessionData.talk.trackId);
            }

            // collect speakers data and add to session and to speakers array
            session.speakers = [];
            let speakers = sessionData.speakers ? sessionData.speakers : sessionData.talk.speakers;
            if (speakers) {
              speakers.forEach((speakerData: any) => {
                session.speakerNames.push(speakerData.name);
                let speakerProfile: Speaker = this.data.speakers.find(
                  (s: any) => s.name === speakerData.name
                );
                if (!speakerProfile) {
                  speakerProfile = new Speaker(speakerData.link.uuid, speakerData.name, '');
                  speakerProfile.sessions = [];
                  speakerProfile.sessions.push(session);

                  session.speakers.push(speakerProfile);
                  this.data.speakers.push(speakerProfile);
                } else {
                  speakerProfile.sessions = speakerProfile.sessions || [];
                  speakerProfile.sessions.push(session);
                }
              });
            }
            
            
          } else {
            session.name = "Slot not yet assigned";
            session.tracks.push('unknown');
          }
        }  

        
      }
    });

    return this.data;
  }

  extractTime(dateAndTime: string) {
    if (dateAndTime && dateAndTime.length === "2019-11-04T11:30:00Z".length)
    return dateAndTime.substr(11, 5);
  }

  getTimeline(
    dayName: string,
    queryText = '',
    excludeTracks: any[] = [],
    segment = 'all'
  ): Observable<any> {
    return this.load().pipe(
      map((data: any) => {
        const schedule = new Schedule(dayName);
        if (dayName === 'all') {
          data.schedule.forEach(day => {
            schedule.groups.push(...day.groups);
          });
        } else {
          const dayIndex = this.data.days.indexOf(dayName);
          schedule.groups.push(...data.schedule[dayIndex].groups);
        }

        schedule.shownSessions = 0;

        queryText = queryText.toLowerCase().replace(/,|\.|-/g, ' ');
        const queryWords = queryText.split(' ').filter(w => !!w.trim().length);

        schedule.groups.forEach((group: any) => {
          group.hide = true;

          group.sessions.forEach((session: any) => {
            // check if this session should show or not
            this.filterSession(session, queryWords, excludeTracks, segment);

            if (!session.hide) {
              // if this session is not hidden then this group should show
              group.hide = false;
              schedule.shownSessions++;
            }
          });
        });

        return schedule;
      })
    );
  }

  filterSession(
    session: any,
    queryWords: string[],
    excludeTracks: any[],
    segment: string
  ) {
    let matchesQueryText = false;
    if (queryWords.length) {
      // of any query word is in the session name than it passes the query test
      queryWords.forEach((queryWord: string) => {
        if (session.name.toLowerCase().indexOf(queryWord) > -1) {
          matchesQueryText = true;
        } else if (session.speakerNames.toString().toLowerCase().indexOf(queryWord) > -1) {
          matchesQueryText = true;
        }
      });
    } else {
      // if there are no query words then this session passes the query test
      matchesQueryText = true;
    }

    // if any of the sessions tracks are not in the
    // exclude tracks then this session passes the track test

    let matchesTracks = false;
    session.tracks.forEach((trackName: string) => {
      if (excludeTracks.indexOf(trackName) === -1) {
        matchesTracks = true;
      }
    });

    // if the segement is 'favorites', but session is not a user favorite
    // then this session does not pass the segment test
    let matchesSegment = false;
    if (segment === 'favorites') {
      if (this.user.hasFavorite(session.name)) {
        matchesSegment = true;
      }
    } else {
      matchesSegment = true;
    }

    // all tests must be true if it should not be hidden
    session.hide = !(matchesQueryText && matchesTracks && matchesSegment);
  }

  getTracks() {
    return this.tracks$;
  }

  getMap() {
    return this.load().pipe(
      map((data: any) => {
        return data.map;
      })
    );
  }
}
