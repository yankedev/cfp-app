import { Injectable } from '@angular/core';
import { Events } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { User } from '../interfaces/user';
import { Observable, from } from 'rxjs';
import { AuthService } from './auth-service';
import { EventDetails } from '../interfaces/eventDetails';
import { VotingService } from '../services/voting-service';

@Injectable({ providedIn: 'root' })
export class UserData {
  _favorites: string[] = [];
  private currentUser$: Observable<User>;

  constructor(
    public events: Events,
    public storage: Storage,
    private authService: AuthService,
    private votingService: VotingService
  ) {
    this.currentUser$ = this.authService.user$;
  }

  async storeSelectedEvent(event: EventDetails) {
    return this.storage.set('selectedEvent', event);
  }

  getStoredEvent(): Observable<EventDetails> {
    return from(this.storage.get('selectedEvent'));
  }

  hasFavorite(sessionName: string): boolean {
    return (this._favorites.indexOf(sessionName) > -1);
  }

  addFavorite(sessionName: string): void {
    this._favorites.push(sessionName);
  }

  removeFavorite(sessionName: string): void {
    const index = this._favorites.indexOf(sessionName);
    if (index > -1) {
      this._favorites.splice(index, 1);
    }
  }

  voteSession(eventId: string, sessionId: string, userId: string, vote: number, comment?: string) {
    this.votingService.vote(eventId, sessionId, userId, vote, comment);
  }

  getSessionVote(eventId: string, sessionId: string, userId: string) {
    return this.votingService.getVote(eventId, sessionId, userId);
  }
}
