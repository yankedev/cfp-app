import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { of, forkJoin, Observable, BehaviorSubject, combineLatest } from 'rxjs';
import { map, mergeMap, tap, filter, retryWhen } from 'rxjs/operators';

import { UserData } from './user-data';

import { Session } from '../interfaces/session';
import { Speaker} from '../interfaces/speaker';
import { Schedule } from '../interfaces/schedule';
import { Group } from '../interfaces/group';
import { SpeakerAdapter } from '../core/speaker-adapter';
import { ConferenceSelector } from './conference-selector';
import { EventDetails } from '../interfaces/eventDetails';
import { Track } from '../interfaces/track';
import { TrackAdapter } from '../core/track-adapter';
import { Talk } from '../interfaces/talk';
import { TalkAdapter } from '../core/talk-adapter';

@Injectable({ providedIn: 'root' })
export class ConferenceService {
  data: any;
  private URL_BASE_PATH = '/api/public';
  private EVENT_URL = this.URL_BASE_PATH + '/event';
  // schedule functionality is still implemented in confinabox-data.ts
  // private SCHEDULES_URL = this.URL_BASE_PATH + '/schedules/';
  private TALKS_URL = this.URL_BASE_PATH + '/talks';
  private TRACKS_URL = this.URL_BASE_PATH + '/tracks';

  private talks$: Observable<Talk[]>;
  private tracks$: Observable<Track[]>;

  constructor(private http: HttpClient,
              private conferenceSelector: ConferenceSelector,
              private talkAdapter: TalkAdapter,
              private speakerAdapter: SpeakerAdapter,
              private trackAdapter: TrackAdapter) {
    this.init();
  }

  init() {

    this.talks$ = this.conferenceSelector.getSelectedEvent$().pipe(
      mergeMap((event: EventDetails) => {
          return this.http.get(this.TALKS_URL).pipe(
            retryWhen(errors$ => errors$.pipe(filter(err => err.status === '404')))
          );
        }
      ),
      // old CFP returns talks list in a map with key talks.accepted
      map((data: any) => data.talks ? data.talks.accepted : data),
      map((talks: any[]) =>
        talks.map(talk => this.talkAdapter.adapt(talk))
      )
    );

    this.tracks$ = this.conferenceSelector.getSelectedEvent$().pipe(
      filter(event => !!event),
      mergeMap((event: EventDetails) => {
        return this.http.get(this.TRACKS_URL);
      }),
      // old CFP returns track list in a map with key=tracks
      map((data: any) => data.tracks ? data.tracks : data),
      map((data: any[]) => data.map(item => this.trackAdapter.adapt(item)))
    );

    // TODO could we do a combineLatest of [getSelectedEvent$, loadTalks$] with loadTalks being an explicit request to reload data?
  }


  getTracks$() {
    return this.tracks$;
  }

  getTalks$() {
    return this.talks$;
  }

  getTalkDetails$(talkId) {
    return this.http.get(this.TALKS_URL + '/' + talkId).pipe(
      map(talk => this.talkAdapter.adapt(talk))
    );
  }

  getEventDetails$(): Observable<EventDetails> {
    // this call is not cached, we execute it because it is small and rarely needed in the app.
    return this.http.get<EventDetails>(this.EVENT_URL);
  }

}
