import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { of, forkJoin, Observable } from 'rxjs';
import { map, mergeMap, tap, filter } from 'rxjs/operators';

import { UserData } from './user-data';

import { Speaker} from '../interfaces/speaker';
import { SpeakerAdapter } from '../core/speaker-adapter';
import { ConferenceSelector } from './conference-selector';
import { EventDetails } from '../interfaces/eventDetails';

@Injectable({ providedIn: 'root' })
export class SpeakersData {
  data: any;
  public eventCode: string;
  public speakers$: Observable<Speaker[]>;
  public conferenceUrl: string;

  private URL_BASE_PATH = '/api/public';
  private SPEAKERS_URL = this.URL_BASE_PATH + '/speakers';

  constructor(public http: HttpClient,
              public user: UserData,
              public conferenceSelector: ConferenceSelector,
              public speakerAdapter: SpeakerAdapter) {
    this.init();
  }

  init() {
    this.speakers$ = this.conferenceSelector.getSelectedEvent$().pipe(
      mergeMap((event: EventDetails) =>
         this.http.get(this.SPEAKERS_URL)
      ),
      map((data: any[]) => data.map(item => this.speakerAdapter.adapt(item))),
      map((speakers: Speaker[]) => {
        return speakers.sort((a: any, b: any) => {
          const aName = a.firstName;
          const bName = b.firstName;
          return aName.localeCompare(bName);
        });
      })
    );
  }

  getSpeakers() {
    return this.speakers$;
  }

  getSpeakerDetails(id: string): Observable<Speaker> {
    return this.conferenceSelector.getSelectedEvent$().pipe(
      mergeMap((event: EventDetails) =>
         this.http.get(this.SPEAKERS_URL + '/' + id)
      ),
      map(item => this.speakerAdapter.adapt(item))
    );
  }

}
