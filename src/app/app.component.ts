import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { SwUpdate } from '@angular/service-worker';

import { Events, MenuController, Platform, ToastController } from '@ionic/angular';

import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { Storage } from '@ionic/storage';

import { ConferenceSelector } from './providers/conference-selector';

import { AuthService } from './providers/auth-service';
import { Observable, of } from 'rxjs';
import { EventDetails } from './interfaces/eventDetails';
import { map } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
  appPages = [
    {
      title: 'Schedule',
      subUrl: '/schedule',
      icon: 'calendar'
    },
    {
      title: 'Speakers',
      subUrl: '/speakers',
      icon: 'contacts'
    },
    {
      title: 'Talks',
      subUrl: '/talks',
      icon: 'mic'
    },
    {
      title: 'Map',
      subUrl: '/map',
      icon: 'map'
    },
    {
      title: 'About',
      subUrl: '/about',
      icon: 'information-circle'
    }
  ];
  loggedIn = false;
  appPages$: Observable<any>;
  selectedEvent$: Observable<EventDetails>;

  constructor(
    private events: Events,
    private menu: MenuController,
    private platform: Platform,
    private router: Router,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private afAuth: AngularFireAuth,
    private authService: AuthService,
    private swUpdate: SwUpdate,
    private toastCtrl: ToastController,
    private conferenceSelector: ConferenceSelector
  ) {
    this.initializeApp();
  }

  async ngOnInit() {
    console.log('Register custom capacitor plugins');

    this.listenForLoginEvents();

    this.swUpdate.available.subscribe(async res => {
      const toast = await this.toastCtrl.create({
        message: 'Update available!',
        showCloseButton: true,
        position: 'bottom',
        closeButtonText: `Reload`
      });

      await toast.present();

      toast
        .onDidDismiss()
        .then(() => this.swUpdate.activateUpdate())
        .then(() => window.location.reload());
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.selectedEvent$ = this.conferenceSelector.getSelectedEvent$();
      this.appPages$ = this.selectedEvent$.pipe(
        map(event => this.generateSubPages(event))
      );

      this.afAuth.authState.subscribe(user => {
          if (user) {
            this.loggedIn = true;
          } else {
            this.loggedIn = false;
          }
        });
    });
  }

  generateSubPages(event: EventDetails) {
    this.appPages.forEach(page => page['url'] = 'app/conferences/' + event.cfpKey + page.subUrl);
    return this.appPages;
  }

  updateLoggedInStatus(loggedIn: boolean) {
    this.loggedIn = loggedIn;
  }

  listenForLoginEvents() {
    this.events.subscribe('user:login', () => {
      this.updateLoggedInStatus(true);
    });

    this.events.subscribe('user:logout', () => {
      this.updateLoggedInStatus(false);
    });
  }

  logout() {
    this.authService.signOut().then(() => {
      if (!this.getCurrentEventId()) {
        return this.router.navigateByUrl('/app/conference-selector');
      } else {
        return this.router.navigateByUrl('/app/conferences/' + this.getCurrentEventId() + '/schedule');
      }
    });
  }

  getAppPages$() {
    return this.appPages$;
  }

  getCurrentEventId() {
    return this.conferenceSelector.getSelectedEvent() ? this.conferenceSelector.getSelectedEvent().cfpKey : null;
  }
}
