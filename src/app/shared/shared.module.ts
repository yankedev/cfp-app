import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {SpeakerAvatarComponent} from './speaker-avatar/speaker-avatar.component';
import { CommonModule } from '@angular/common';
import {IonicModule} from '@ionic/angular';

@NgModule({
  imports: [
    CommonModule, IonicModule
  ],
  declarations: [
    SpeakerAvatarComponent
  ],
  exports: [
    SpeakerAvatarComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ScheduleSharedModule {
  static forRoot() {
    return {
      ngModule: ScheduleSharedModule
    };
  }
}
