import {Component, Input, OnInit} from '@angular/core';
import {Speaker} from '../../interfaces/speaker';
import {ConferenceData} from '../../providers/confinabox-data';
import {Storage} from '@ionic/storage';
import { SpeakersData } from '../../providers/speakers-data';

@Component({
  selector: 'speaker-avatar',
  templateUrl: './speaker-avatar.component.html',
  styleUrls: ['speaker-avatar.scss']
})
export class SpeakerAvatarComponent implements OnInit {

  @Input() speakerId: string;
  @Input() smallIcon: false;
  @Input() dropDownView: boolean;

  speakersCache: Speaker[] = [];

  speaker: Speaker;

  constructor(private dataProvider: SpeakersData,
              private storage: Storage) {
  }

  ngOnInit(): void {
    this.storage.get(this.speakerId).then( (res: Speaker) => {
      if (res) {
        this.speakersCache.push(res);
        this.speaker = res;
      } else {
        this.dataProvider.getSpeakerDetails(this.speakerId)
          .subscribe( (speaker: Speaker) => {
            this.speakersCache.push(speaker);
            this.storage.set(speaker.id, speaker);
            this.speaker = speaker;
          });
      }
    });
  }
}
