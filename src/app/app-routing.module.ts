import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './core/auth-guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/conference-selector',
    pathMatch: 'full'
  },
  {
    // this page is called when PWA is started from launch icon
    // TODO we should go to the last opened page (see https://gitlab.com/yankedev/cfp-app/issues/42)
    path: 'index.html',
    redirectTo: '/conference-selector',
  },
  {
    path: 'account',
    loadChildren: () =>
      import('./pages/account/account.module').then(m => m.AccountModule)
  },
  {
    path: 'login',
    loadChildren: () =>
      import('./pages/login/login.module').then(m => m.LoginModule)
  },
  {
    path: 'app',
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('./pages/tabs-page/tabs-page.module').then(m => m.TabsModule)
  },
  {
    path: 'conference-selector',
    loadChildren: () =>
      import('./pages/conference-selector/conference-selector.module').then(
        m => m.ConferenceSelectorModule
      )
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
