import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { ConferenceSelector } from '../providers/conference-selector';
import { EventDetails } from '../interfaces/eventDetails';
import { map, mergeMap } from 'rxjs/operators';
import { AuthService } from '../providers/auth-service';

@Injectable()
export class AddDomainOnDeviceInterceptor implements HttpInterceptor {

  constructor(public conferenceSelector: ConferenceSelector, private authService: AuthService) {
  }

  // from mobile app (published on the AppStore or PlayStore) we can call any domain
  // when the app is running as PWA in a browser we can call only domains allowing CrossOrigin, all the others are proxied at /api
  private allowedCrossOriginDomains = ['www.devoxxians.com'];

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let apiUrl = req.url;
    let newHeaders = req.headers;

    if (this.useRelativeUrlForAPICall(req)) {
      // remove domain from apiUrl 
      apiUrl = req.url.substring(req.url.indexOf('/api/'));

      // since the api url is now relative, we rewrite /api urls to include eventCode to be able to proxy to different servers
      // i.e /api/public/schedule => /api/public/conferences/dvbe19/schedule
      apiUrl = apiUrl.replace('/api/', '/api/conferences/' + this.getCurrentEventCode() + '/');

      newHeaders = this.appendFirebaseUserHttpHeader(newHeaders);
    }

    const newReq = req.clone({ url: apiUrl, headers: newHeaders});
    return next.handle(newReq);
  }

  useRelativeUrlForAPICall(req: HttpRequest<any>): boolean {
    // /api/public/events are special urls proxied to devoxxians.com
    if (req.url.indexOf('/api/public/events/') >= 0) return false;

    const matches = req.url.match('.*\://?([^\/]+)');

    // if matches is null req.url is a relative url
    if (!matches) { return true; }

    // if the domain is in the array we call it directly
    // TODO if running in a mobile app we should return always false.
    if (this.allowedCrossOriginDomains.indexOf(matches[1]) >= 0) {
      return false;
    }
    return true;
  }

  getCurrentEventCode() {
    return this.conferenceSelector.getSelectedEvent().cfpKey;
  }

  appendFirebaseUserHttpHeader(headers: HttpHeaders): HttpHeaders { 
    let idToken = this.authService.getFirebaseToken();
    if (!idToken) {
      return  headers;
    }

    return headers.append('X-Firebase-Auth', idToken);
  }
}
