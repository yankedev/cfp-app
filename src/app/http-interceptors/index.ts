/* "Barrel" of Http Interceptors */
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { AddDomainOnDeviceInterceptor } from './add-domain-on-device-interceptor';

/** Http interceptor providers in outside-in order */
export const httpInterceptorProviders = [
  {
    provide: HTTP_INTERCEPTORS,
    useClass: AddDomainOnDeviceInterceptor,
    multi: true
  }
];
