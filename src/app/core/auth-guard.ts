import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, Route } from '@angular/router';
import { Observable, of, from } from 'rxjs';
import { ConferenceSelector } from '../providers/conference-selector';
import { map } from 'rxjs/operators';
import { UserData } from '../providers/user-data';

@Injectable()
export class AuthGuard implements CanActivate {


  constructor(private _router: Router, private conferenceSelector: ConferenceSelector, userData: UserData) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>  {
    const selectedEventId = this.conferenceSelector.getSelectedEvent() ? this.conferenceSelector.getSelectedEvent().cfpKey : null;
    const urlEventId = this.getEventId(state.url);

    // if somebody is pointing to an url with the eventId but no conference is selected
    // it may happens when clicking on a bookmark, or making a refresh of the page
    if (!selectedEventId && urlEventId) {
      // we set the event from url (asynch) and then we got the url requested
      return this.conferenceSelector.chooseEvent(urlEventId).pipe(
        map(chosenEvent => true)
      );
    } else if (!selectedEventId && !urlEventId) {
      this._router.navigate(['/conference-selector']);
      return of(false);
    } else if (selectedEventId && !urlEventId) {
      this._router.navigate(['/app/conferences/' + selectedEventId + '/schedule']);
      return of(false);
    } else if (selectedEventId === urlEventId) {
      return of(true);
    }

    this._router.navigate(['/conference-selector']);
    return of(false);
  }

  getEventId(url: string): string {
    const routeSegments = url.split('/');
    if (routeSegments && routeSegments[2] === 'conferences') {
      const eventId = routeSegments[3];
      if (eventId === 'null' || eventId === 'default') {
        return null;
      }
      return eventId;
    }
    return null;
  }

}
