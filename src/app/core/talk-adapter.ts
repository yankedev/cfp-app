import { Injectable } from '@angular/core';
import { Adapter } from './adapter';
import { Talk } from '../interfaces/talk';
import { SpeakerAdapter } from './speaker-adapter';
import { TimeSlot } from '../interfaces/time-slot';

@Injectable({
    providedIn: 'root'
  })
export class TalkAdapter implements Adapter<Talk> {

    constructor(private speakerAdapter: SpeakerAdapter) {}

    adapt(item: any): Talk {
        const talk = new Talk();
        talk.id = item.id;
        talk.title = item.title ? item.title : item.name;
        talk.track = item.trackName ? item.trackName : item.track;
        talk.trackId = item.trackId;
        talk.summary = item.description ? item.description : item.summary;

        talk.durationInMinutes = item.sessionTypeDuration;
        talk.type = item.sessionTypeName;

        const speakers: any[] = item.speakers ? item.speakers : [{firstName: item.mainSpeaker}];

        talk.speakers = speakers.map(speaker => this.speakerAdapter.adapt(speaker));

        if (item.timeSlots) {
            talk.timeSlots = item.timeSlots.map(timeslot => this.adaptTimeSlot(timeslot));

        }
        if (item.tags) {
            talk.tags = item.tags
                            .filter(tag => tag.name || tag.value)
                            .map(tag => tag.name ? tag.name : tag.value);
        }
        return talk;
    }

    adaptTimeSlot(item) {
        let slot = new TimeSlot();
        slot.roomId = item.roomId;
        slot.roomName = item.roomName;
        slot.timeStart = item.timeStart;
        slot.timeEnd = item.timeEnd;
        slot.timeZone = item.timeZone;
        return slot;
    }
}
