import { Injectable } from '@angular/core';
import { Adapter } from './adapter';
import { Speaker } from '../interfaces/speaker';

@Injectable({
    providedIn: 'root'
  })
export class SpeakerAdapter implements Adapter<Speaker> {

    adapt(item: any): Speaker {
        const id = item.uuid ? item.uuid : item.id;
        const newSpeaker = new Speaker(
            id,
            item.firstName,
            item.lastName,
        );

        newSpeaker.imageURL = item.avatarURL ? item.avatarURL : item.imageUrl;
        newSpeaker.company = item.company;
        newSpeaker.twitter = item.twitter ? item.twitter : item.twitterHandle;
        newSpeaker.bio = item.bioAsHtml ? item.bioAsHtml : item.bio;
        return newSpeaker;
    }
}
