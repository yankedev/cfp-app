import { Injectable } from '@angular/core';
import { Adapter } from './adapter';
import { Track } from '../interfaces/track';

@Injectable({
    providedIn: 'root'
  })
export class TrackAdapter implements Adapter<Track> {

    adapt(item: any): Track {
        const track = new Track();
        track.id = item.id;
        track.title = item.title ? item.title : item.name;
        track.description = item.description;
        track.iconUrl = item.imgsrc ? item.imgsrc : item.imageURL;
        return track;
    }
}
