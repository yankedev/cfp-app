import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConferenceData } from '../../providers/confinabox-data';
import { Observable } from 'rxjs';
import { Speaker } from '../../interfaces/speaker';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { SpeakersData } from '../../providers/speakers-data';

@Component({
  selector: 'page-speaker-detail',
  templateUrl: 'speaker-detail.html',
  styleUrls: ['./speaker-detail.scss'],
})
export class SpeakerDetailPage {
  // speaker: any;
  speakerDetail$: Observable<Speaker>;

  constructor(
    private dataProvider: SpeakersData,
    private router: Router,
    private route: ActivatedRoute,
    public inAppBrowser: InAppBrowser
  ) {}

  ionViewWillEnter() {
    const speakerId = this.route.snapshot.paramMap.get('speakerId');
    this.speakerDetail$ = this.dataProvider.getSpeakerDetails(speakerId);
/*
    this.dataProvider.load().subscribe((data: any) => {
      const speakerId = this.route.snapshot.paramMap.get('speakerId');
      if (data && data.speakers) {
        for (const speaker of data.speakers) {
          if (speaker && speaker.id === speakerId) {
            this.speaker = speaker;
            break;
          }
        }
      }
    });*/
  }

  goToSpeakerTwitter(speaker: any) {
    this.inAppBrowser.create(
      `https://twitter.com/${speaker.twitter}`,
      '_blank'
    );
  }
}
