import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { UserData } from '../../providers/user-data';

import { AuthService } from '../../providers/auth-service';
import { IonRouterOutlet, NavController } from '@ionic/angular';



@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  styleUrls: ['./login.scss'],
})
export class LoginPage {

  constructor(
    public userData: UserData,
    public router: Router,
    private navCtrl: NavController,
    public authService: AuthService,
  ) { }


  async doGoogleLogin() {
    this.authService.googleSignin().then(() => {
      this.navCtrl.pop();
    });

  }

  doGithubLogin() {
    this.authService.githubSignin().then(() => {
      this.navCtrl.pop();
    });

  }

  async doTwitterLogin() {
    this.authService.twitterSignin().then(() => {
      this.navCtrl.pop();
    });

  }

  logout() {
    this.authService.signOut();
  }

  ionViewWillEnter() {
  }

}
