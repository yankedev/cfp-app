import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { tap } from 'rxjs/operators';
import { ConferenceSelector } from '../../providers/conference-selector';
import { Subscription } from 'rxjs';

@Component({
  templateUrl: 'tabs-page.html'
})
export class TabsPage implements OnInit, OnDestroy {

  eventIdSubscription: Subscription;

  constructor(private route: ActivatedRoute,
              private conferenceSelector: ConferenceSelector) {
  }

  ngOnInit() {
    this.eventIdSubscription = this.route.paramMap.pipe(
      tap(params => console.log('Loading tabs page for event: ' + params.get('eventId')))
    ).subscribe();
  }

  ngOnDestroy() {
    this.eventIdSubscription.unsubscribe();
  }

  chooseEvent(eventId: string) {

    this.conferenceSelector.chooseEvent(eventId);
  }
}
