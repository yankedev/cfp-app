import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TalkListPage } from './talk-list.page';

describe('TalkListPage', () => {
  let component: TalkListPage;
  let fixture: ComponentFixture<TalkListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TalkListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TalkListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
