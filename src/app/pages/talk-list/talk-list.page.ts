import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ConferenceService } from '../../providers/conference-service';
import { Observable, BehaviorSubject, combineLatest } from 'rxjs';
import { Talk } from '../../interfaces/talk';
import { Track } from '../../interfaces/track';
import { map, tap } from 'rxjs/operators';
import { Speaker } from '../../interfaces/speaker';
import { IonList, ModalController, IonInfiniteScroll } from '@ionic/angular';
import { ScheduleFilterPage } from '../schedule-filter/schedule-filter';

@Component({
  selector: 'talk-list',
  templateUrl: './talk-list.page.html',
  styleUrls: ['./talk-list.page.scss'],
})
export class TalkListPage implements OnInit, OnDestroy {
  // Gets a reference to the list element
  @ViewChild('scheduleList', { static: true }) scheduleList: IonList;
  excludeTracks: any = [];
  excludeTracks$ = new BehaviorSubject<any[]>([]);

  @ViewChild('infinitescroll', {static: true})
  infiniteScroll: IonInfiniteScroll;
  checkDisplayMore;

  private talks$: Observable<Talk[]>;
  public filteredTalks$: Observable<Talk[]>;
  public tracks$: Observable<Track[]>;
  public tracksIconById: {[id:string]: string};
  public searchQuery = ''; // holds the value there is in the search box
  private searchQuery$: BehaviorSubject<string>;
  public searchChips: Map<number, any>;
  public visibleTalks = 10;
  public foundTalksCount = 0;

  constructor(private conferenceService: ConferenceService, public modalCtrl: ModalController) { }

  ngOnInit() {
    this.talks$ = combineLatest([this.conferenceService.getTalks$(), this.excludeTracks$]).pipe(
      map(([talks, excludedTracks]) => {
        return talks.filter(talk => excludedTracks.indexOf(talk.track) < 0);
      })
    );
    this.tracks$ = this.conferenceService.getTracks$();

    this.tracks$.subscribe(tracks => {
      this.tracksIconById = {};
      tracks.forEach(track => {
        this.tracksIconById[track.id] = track.iconUrl;
      });
    });

    this.searchQuery$ = new BehaviorSubject(this.searchQuery);

    this.filteredTalks$ = combineLatest([this.talks$, this.searchQuery$]).pipe(
      tap(([talks, searchQuery]) => {
        this.foundTalksCount = talks.length;
      }),
      map(([talks, searchQuery]) => {
        // here we imperatively implement the filtering logic
        if (searchQuery === '') { return talks; }
        const q = searchQuery.toLowerCase();
        return talks.filter(
          talk => { return (this.findInTitle(talk, q) ||
                            this.findInSpeakers(talk.speakers, q) ||
                            this.findInTags(talk.tags, q)); }
        );
      }),
      tap(() => {
        this.displayMoreIfNecessary();
      })
    );
    this.searchChips =  new Map([[1, {id: 'title', active: true, label: 'Title'}],
                                  [2, {id: 'speakerName', active: true, label: 'Speaker Name'}],
                                  [3, {id: 'speakerCompany', active: true, label: 'Speaker Company'}],
                                  [4, {id: 'tags', active: true, label: 'Tags'}],
                                ]);

    window.addEventListener('resize', () => this.displayMoreIfNecessary());
  }

  ngOnDestroy() {
    window.removeEventListener('resize', () => this.displayMoreIfNecessary());
  }

  getChips(): Map<number, any> {
    return this.searchChips;
  }

  toggleChip(chipId) {
    const chip = this.searchChips.get(chipId);
    chip.active = !chip.active;
    // re-execute search
    this.searchQuery$.next(this.searchQuery);
  }

  findInTitle(talk, q): boolean {
    if (!this.isSearchForTitleActive()) { return false; }
    return (talk.title.toLowerCase().indexOf(q) >= 0);
  }

  findInSpeakers(speakers: Speaker[], q): boolean {
    const foundSpeaker = speakers.find(speaker => (
          (this.isSearchForNameActive() && (speaker.getDisplayName().toLowerCase().indexOf(q) >= 0)) ||
          (this.isSearchForCompanyActive() && (speaker.company && speaker.company.toLowerCase().indexOf(q) >= 0))
      ));

    return (!!foundSpeaker);
  }

  findInTags(tags: string[], q): boolean {
    if (!this.isSearchForTagsActive()) { return false; }
    const foundTag = (tags.find(tag => (tag.toLowerCase().indexOf(q) >= 0)));

    return (!!foundTag);
  }

  isSearchForTitleActive() {
    return this.searchChips.get(1).active;
  }

  isSearchForNameActive() {
    return this.searchChips.get(2).active;
  }

  isSearchForCompanyActive() {
    return this.searchChips.get(3).active;
  }

  isSearchForTagsActive() {
    return this.searchChips.get(4).active;
  }

  async presentFilter() {
    const modal = await this.modalCtrl.create({
      component: ScheduleFilterPage,
      componentProps: { excludedTracks: this.excludeTracks }
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    if (data) {
      this.excludeTracks = data;
      this.excludeTracks$.next(this.excludeTracks);
    }
  }

  changeVisibleTalks(selectedValue) {
    this.searchQuery$.next(selectedValue);
  }

  loadData(event) {
      this.visibleTalks = Math.min(this.visibleTalks + 10, this.foundTalksCount);
      event.target.complete();
  }

  displayMoreIfNecessary() {
    clearTimeout(this.checkDisplayMore)
    this.checkDisplayMore = setTimeout(() => {
      if(this.isInViewPort((<any> this.infiniteScroll).el)) {
        this.visibleTalks = Math.min(this.visibleTalks+10, this.foundTalksCount);
        this.displayMoreIfNecessary();
      }
    }, 100);
  }

  isInViewPort(elem: HTMLElement): boolean {
    var distance = elem.getBoundingClientRect();
    return (
      distance.top >= 0 &&
      distance.left >= 0 &&
      distance.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
      distance.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
  };

}
