import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SessionDetailPage } from './session-detail';
import { SessionDetailPageRoutingModule } from './session-detail-routing.module';
import { IonicModule } from '@ionic/angular';
import {ScheduleSharedModule} from '../../shared/shared.module';
import { VotingModalPage } from '../voting-modal/voting-modal.page';
import { VotingModalPageModule } from '../voting-modal/voting-modal.module';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    SessionDetailPageRoutingModule,
    ScheduleSharedModule,
    VotingModalPageModule
  ],
  declarations: [
    SessionDetailPage
  ]
})
export class SessionDetailModule { }
