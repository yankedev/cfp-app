import { Component, OnInit } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { UserData } from '../../providers/user-data';
import { Speaker } from '../../interfaces/speaker';
import { ConferenceService } from '../../providers/conference-service';
import { Observable } from 'rxjs';
import { Talk } from '../../interfaces/talk';
import { tap, mergeMap, map, first } from 'rxjs/operators';
import { AuthService } from '../../providers/auth-service';
import { IonRouterOutlet, ModalController } from '@ionic/angular';
import { ConferenceSelector } from '../../providers/conference-selector';
import { VotingModalPage } from '../voting-modal/voting-modal.page';
import { OverlayEventDetail } from '@ionic/core';
import { Track } from '../../interfaces/track';

@Component({
  selector: 'page-session-detail',
  styleUrls: ['./session-detail.scss'],
  templateUrl: 'session-detail.html'
})
export class SessionDetailPage implements OnInit {
  session$: Observable<Talk>;
  eventId: string;
  sessionId: string;
  session: Talk;
  track: Track;
  isFavorite = false;
  defaultHref = '';
  userRating$: Observable<number>;

  canGoBack: boolean;
  speakers: Speaker[] = [];

  constructor(
    private conferenceService: ConferenceService,
    private conferenceSelector: ConferenceSelector,
    private userProvider: UserData,
    private authService: AuthService,
    private route: ActivatedRoute,
    private ionRouterOutlet: IonRouterOutlet,
    private modalController: ModalController
  ) {
  }

  ngOnInit() {
    this.canGoBack = this.ionRouterOutlet.canGoBack();
  }

  isLoggedIn() {
    return !!this.authService.getCurrentUserId();
  }

  toggleFavorite() {
    if (this.userProvider.hasFavorite(this.session.id)) {
      this.userProvider.removeFavorite(this.session.id);
      this.isFavorite = false;
    } else {
      this.userProvider.addFavorite(this.session.id);
      this.isFavorite = true;
    }
  }

  ionViewWillEnter() {
    this.eventId = this.conferenceSelector.getSelectedEvent().cfpKey;
    this.sessionId = this.route.snapshot.paramMap.get('sessionId');
    this.session$ = this.conferenceService.getTalkDetails$(this.sessionId).pipe(
      tap(talk => this.conferenceService.getTracks$().subscribe(tracks => this.track = tracks.filter(track => track.id === talk.trackId)[0])),
      tap(talk => this.session = talk)
    );

    this.userRating$ = this.authService.user$.pipe(
      mergeMap(user => this.userProvider.getSessionVote(this.eventId, this.sessionId, user.id)),
      map((vote: any) => {
        return vote ? vote.rating : null;
      })
    );
  }

  async openModal(event) {
    event.stopPropagation();
    const modal: HTMLIonModalElement =
       await this.modalController.create({
          component: VotingModalPage,
          componentProps: {
             eventId: this.eventId,
             sessionId: this.sessionId,
             aParameter: true,
             otherParameter: new Date()
          }
    });

    modal.onDidDismiss().then((detail: OverlayEventDetail) => {
       if (detail !== null) {
         console.log('The result:', detail.data);
       }
    });

    await modal.present();
}
}
