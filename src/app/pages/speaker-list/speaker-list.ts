import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { ActionSheetController } from '@ionic/angular';

import { Observable, BehaviorSubject, combineLatest } from 'rxjs';
import { Speaker } from '../../interfaces/speaker';
import { ConferenceSelector } from '../../providers/conference-selector';
import { SpeakersData } from '../../providers/speakers-data';
import { map } from 'rxjs/operators';

@Component({
  selector: 'page-speaker-list',
  templateUrl: 'speaker-list.html',
  styleUrls: ['./speaker-list.scss'],
})
export class SpeakerListPage {
  filteredSpeakers$: Observable<Speaker[]>;
  private searchQuery$: BehaviorSubject<string>;
  public searchQuery = ''; // holds the value there is in the search box
  
  constructor(
    public actionSheetCtrl: ActionSheetController,
    public confData: SpeakersData,
    public conferenceSelector: ConferenceSelector,
    public inAppBrowser: InAppBrowser,
    public router: Router
  ) {}

  ionViewDidEnter() {
    this.searchQuery$ = new BehaviorSubject(this.searchQuery);

    this.filteredSpeakers$ = combineLatest([this.confData.getSpeakers(), this.searchQuery$]).pipe(
      map(([speakers, searchText]) => {
        // here we imperatively implement the filtering logic
        if (searchText === '') { return speakers; }
        const q = searchText.toLowerCase();
        return speakers.filter(
          speaker => this.filterSpeaker(speaker, q)
        );
      })
    );
  }

  filterSpeaker(speaker: Speaker, q): boolean {
    return ((speaker.getDisplayName().toLowerCase().indexOf(q) >= 0) ||
            (speaker.company && speaker.company.toLowerCase().indexOf(q) >= 0));
  }

  goToSpeakerTwitter(speaker: any) {
    this.inAppBrowser.create(
      `https://twitter.com/${speaker.twitter}`,
      '_blank'
    );
  }

  headerFnExtractFirstLetter(record: Speaker, recordIndex, records: Speaker[]) {
    if (recordIndex != 0 && record.firstName.charAt(0) === records[recordIndex-1].firstName.charAt(0)) {
      return null;
    }

    return record.firstName.charAt(0);
  }

  changeVisibleSpeakers(selectedValue) {
    this.searchQuery$.next(selectedValue);
  }
}
