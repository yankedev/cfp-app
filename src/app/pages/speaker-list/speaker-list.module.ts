import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';

import { SpeakerListPage } from './speaker-list';
import { SpeakerListPageRoutingModule } from './speaker-list-routing.module';
import { FormsModule } from '@angular/forms';
import { SpeakerItem } from '../speaker-item/speaker-item';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SpeakerListPageRoutingModule,
  ],
  declarations: [SpeakerListPage, SpeakerItem],
})
export class SpeakerListModule {}
