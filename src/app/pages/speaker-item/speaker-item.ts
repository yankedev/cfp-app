import { Component, Input } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

import { Speaker } from '../../interfaces/speaker';

@Component({
  selector: 'speaker-item',
  templateUrl: 'speaker-item.html',
  styleUrls: ['./speaker-item.scss'],
})
export class SpeakerItem {
  @Input()
  speaker: Speaker;

  constructor(public inAppBrowser: InAppBrowser) {}

  goToSpeakerTwitter(speaker: any) {
    this.inAppBrowser.create(
      `https://twitter.com/${speaker.twitter}`,
      '_blank'
    );
  }

}
