import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { PopoverPage } from '../about-popover/about-popover';
import { ConferenceData } from '../../providers/confinabox-data';
import { EventDetails } from '../../interfaces/eventDetails';
import { ConferenceSelector } from '../../providers/conference-selector';
import { Observable } from 'rxjs';
import { ConferenceService } from '../../providers/conference-service';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
  styleUrls: ['./about.scss'],
})
export class AboutPage implements OnInit {

  eventDetails$: Observable<EventDetails>;

  conferenceFromDate = '2047-05-17';
  conferenceToDate = '2047-05-17';

  constructor(public popoverCtrl: PopoverController,
              private conferenceServer: ConferenceService) { }


  ngOnInit(): void {
    this.eventDetails$ = this.conferenceServer.getEventDetails$();
  }

  async presentPopover(event: Event) {
    const popover = await this.popoverCtrl.create({
      component: PopoverPage,
      event
    });
    await popover.present();
  }
}
