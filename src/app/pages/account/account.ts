import { AfterViewInit, Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

import { AlertController } from '@ionic/angular';

import { UserData } from '../../providers/user-data';
import { Observable } from 'rxjs';
import { User } from '../../interfaces/user';
import { AuthService } from '../../providers/auth-service';


@Component({
  selector: 'page-account',
  templateUrl: 'account.html',
  styleUrls: ['./account.scss'],
})
export class AccountPage implements AfterViewInit {
  user$: Observable<User>;

  constructor(
    public router: Router,
    public authService: AuthService
  ) { }

  ngAfterViewInit() {
    this.user$ = this.authService.user$;
  }

  logout() {
    this.authService.signOut().then(data => {
      this.router.navigateByUrl('/login');
    });
  }
}
