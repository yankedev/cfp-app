import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VotingModalPage } from './voting-modal.page';

describe('VotingModalPage', () => {
  let component: VotingModalPage;
  let fixture: ComponentFixture<VotingModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VotingModalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VotingModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
