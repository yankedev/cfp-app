import { Component, OnInit } from '@angular/core';
import { ConferenceSelector } from '../../providers/conference-selector';
import { UserData } from '../../providers/user-data';
import { AuthService } from '../../providers/auth-service';
import { ModalController, NavParams } from '@ionic/angular';
import { Observable, Subscription } from 'rxjs';
import { ToastService } from '../../services/toast.service';

@Component({
  selector: 'voting-modal',
  templateUrl: './voting-modal.page.html',
  styleUrls: ['./voting-modal.page.scss'],
})
export class VotingModalPage {

  votes = [
    {
      'rating' : 1,
      'data' : [
        {
          'text' : 'Difficult to understand',
          'imageUrl' : ''
        },
        {
          'text' : 'Too fast',
          'imageUrl' : ''
        },
        {
          'text' : 'Not enough demos/samples',
          'imageUrl' : ''
        },
        {
          'text' : 'Complicated',
          'imageUrl' : ''
        }
      ]
    },
    {
      'rating' : 2,
      'data' : [
        {
          'text' : 'Difficult to understand',
          'imageUrl' : ''
        },
        {
          'text' : 'Too fast',
          'imageUrl' : ''
        },
        {
          'text' : 'Not enough demos/samples',
          'imageUrl' : ''
        },
        {
          'text' : 'Complicated',
          'imageUrl' : ''
        }
      ]
    },
    {
    'rating' : 3,
    'data' : [
      {
        'text' : 'FUN!',
        'imageUrl' : ''
      },
      {
        'text' : 'I loved the demos',
        'imageUrl' : ''
      },
      {
        'text' : 'Learned something new',
        'imageUrl' : ''
      },
      {
        'text' : 'Very interesting',
        'imageUrl' : ''
      }
    ]
    },
    {
    'rating' : 4,
    'data' : [
      {
        'text' : 'FUN!',
        'imageUrl' : ''
      },
      {
        'text' : 'I loved the demos',
        'imageUrl' : ''
      },
      {
        'text' : 'Learned something new',
        'imageUrl' : ''
      },
      {
        'text' : 'Very interesting',
        'imageUrl' : ''
      }
    ]
    },
    {
    'rating' : 5,
    'data' : [
      {
        'text' : 'Awesome content',
        'imageUrl' : ''
      },
      {
        'text' : 'Really enjoyed this',
        'imageUrl' : ''
      },
      {
        'text' : 'Amazing speaker',
        'imageUrl' : ''
      },
      {
        'text' : 'Devoxx Rock Star is born',
        'imageUrl' : ''
      },
      {
        'text' : 'New Venkat in the making',
        'imageUrl' : ''
      }
    ]
    }];
  rating: number;
  comment: string;
  comments: any[];
  sessionId: string;
  eventId: string;
  currentUserId: string;
  userVoteSubscription: Subscription;

  constructor(private conferenceSelector: ConferenceSelector,
              private userProvider: UserData,
              private authService: AuthService,
              private modalController: ModalController,
              private navParams: NavParams,
              private toast: ToastService) { }

  ionViewWillEnter() {
    this.eventId = this.navParams.get('eventId');
    this.sessionId = this.navParams.get('sessionId');
    this.currentUserId = this.authService.getCurrentUserId();
    this.userVoteSubscription = this.userProvider.getSessionVote(this.eventId, this.sessionId, this.currentUserId)
                                                 .subscribe((vote: any) => {
                                                   if (!vote) {
                                                    this.rating = null;
                                                    this.comment = null;
                                                   } else {
                                                    this.rating = vote.rating;
                                                    this.comment = vote.comment;
                                                   }
                                                 });
  }

  saveVote() {
      this.userProvider.voteSession(this.eventId, this.sessionId, this.currentUserId, this.rating, this.comment);
      this.toast.showSuccessToast('Thanks for your feedback');
      this.dismiss();
  }


  selectVoteComments(vote: number) {
    if (vote !== this.rating) {
      this.rating = vote;
      this.comment = '';
    }
    this.comments = this.votes[vote - 1].data;
  }

  setComment(text) {
    this.comment = text;
  }

  async dismiss() {
    this.userVoteSubscription.unsubscribe();
    const result: Date = new Date();

    await this.modalController.dismiss(result);
  }
}
