import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConferenceSelectorPage } from './conference-selector';

describe('ConferenceSelectorPage', () => {
  let component: ConferenceSelectorPage;
  let fixture: ComponentFixture<ConferenceSelectorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConferenceSelectorPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConferenceSelectorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
