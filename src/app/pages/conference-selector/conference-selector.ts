import { Component, OnInit } from '@angular/core';
import { ConferenceSelector } from '../../providers/conference-selector';
import { Observable, BehaviorSubject, combineLatest, of } from 'rxjs';
import { EventDetails } from '../../interfaces/eventDetails';
import { filter, map, tap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'conference-selector',
  templateUrl: './conference-selector.html',
  styleUrls: ['./conference-selector.scss'],
})
export class ConferenceSelectorPage implements OnInit {
  segment: string;
  filter$: BehaviorSubject<string>;
  events$: Observable<EventDetails[]>;
  eventsCounter: number;
  devoxxEventsCounter: number;
  voxxedEventsCounter: number;

  filteredEvents$: Observable<EventDetails[]>;

  showPastEvents: false;

  constructor(private conferenceSelector: ConferenceSelector,
              public router: Router) { }

  ngOnInit() {
    this.segment = 'all';
    // We create a stream ourselves to map an event form the child
    // component to a stream of 'filter values'.
    // We use a BehaviorSubject because this will have an initial
    // value. This is important because the combineLatest operator
    // we will use below only works if every stream has emitted
    // at least one value.
    this.filter$ = new BehaviorSubject(this.segment);

    // we keep the stream containing all the visible events
    this.events$ = this.conferenceSelector.getEventList().pipe(
                      map((events: any[]) => events.filter(event => !this.isPastEvent(event))),
                      tap(events => this.countEvents(events))
                    );

    // we create a new stream based on the two input streams we defined
    this.filteredEvents$ = this.createFilterEvents(this.filter$, this.events$);
  }

  countEvents(events: EventDetails[]) {
    this.eventsCounter = events.length;
    this.devoxxEventsCounter = events.filter(
      (event: EventDetails) => event.eventCategory.toLowerCase() === 'devoxx').length;
    this.voxxedEventsCounter = events.filter(
      (event: EventDetails) => event.eventCategory.toLowerCase() === 'voxxed').length;
  }

  public createFilterEvents(filter$: Observable<string>, eventList$: Observable<EventDetails[]>) {
    // We combine both of the input streams using the combineLatest
    // operator. Every time one of the two streams we are combining
    // here changes value, the project function is re-executed and
    // the result stream will get a new value. In our case this is
    // a new array with all the filtered characters.
    return combineLatest([eventList$, filter$], (eventList: EventDetails[], filterText: string) => {
              // here we imperatively implement the filtering logic
              if (filterText === 'all') { return eventList; }
              return eventList.filter(
                event => event.eventCategory.toLowerCase() === filterText.toLowerCase()
              );
    });
  }

  isPastEvent(event: EventDetails) {
    // this is temporary because we always want to see devoxxuk19
    if (event.cfpKey === 'dvbe19') { return false; }

    const eventEndDate = new Date(event.toDate);

    // TODO: we should also show events finished in the last 3 days
    // currently an event finishing today at 16 is calcolated as not being in the past (whatever hour is now)
    const lastFutureDate = new Date();
    lastFutureDate.setHours(0);
    lastFutureDate.setMinutes(0);
    if (eventEndDate < lastFutureDate) {
      return true;
    }
    return false;
  }

  changeVisibleEvents(selectedValue) {
    this.filter$.next(selectedValue);
  }

  selectEvent(event: EventDetails) {
    this.conferenceSelector.setSelectedEvent(event);
    this.router.navigateByUrl('/app/conferences/' + event.cfpKey + '/speakers');
  }


  eventExist(event: EventDetails): Observable<boolean> {
    return this.conferenceSelector.eventExist(event);
  }
}
