export const environment = {
  production: true,
  developmentMode: false,
  firebase: {
    apiKey: 'AIzaSyCuB8Re_MauJqpWQECyOoB35NYyGKidBA8',
    authDomain: 'cfp-dev-app.firebaseapp.com',
    databaseURL: 'https://cfp-dev-app.firebaseio.com',
    projectId: 'cfp-dev-app',
    storageBucket: 'cfp-dev-app.appspot.com',
    messagingSenderId: '615443238699',
    appId: '1:615443238699:web:a941b47ec0644d2f'
  }
};
